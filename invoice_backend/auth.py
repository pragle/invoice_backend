#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from django.contrib.sessions.middleware import SessionMiddleware


class NoCookieSessionMiddleware(SessionMiddleware):

    def process_response(self, request, response):
        response = super(NoCookieSessionMiddleware, self).process_response(request, response)
        for k in list(response.cookies.keys()):
            del response.cookies[k]
        return response


class EmailBackend(ModelBackend):
    counter = 0
    
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=username)
        except UserModel.DoesNotExist:
            raise
        else:
            if user.check_password(password):
                return user
        return None