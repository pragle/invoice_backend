#!/usr/bin/env python
# -*- coding: utf-8 -*-
from graphene_django import DjangoObjectType
from graphql_relay.node.node import from_global_id
from graphene_django.filter import DjangoFilterConnectionField
from invoice_backend import models
import graphene
import decimal

# --------------------------------------------------
# -- CLIENT
# --------------------------------------------------

class Client(DjangoObjectType):
    class Meta:
        model = models.Client
        interfaces = (graphene.Node, )
        filter_fields = {
            'name': ['exact', 'icontains', 'istartswith'],
            'signature': ['exact', 'icontains', 'istartswith'],
            'data': ['exact', 'icontains', 'istartswith'],
            'bank_account': ['exact', 'icontains', 'istartswith'],
        }


class ClientInput(graphene.InputObjectType):
    name = graphene.String()
    signature = graphene.String()
    data = graphene.String()
    bank_account = graphene.String()


class ClientAdd(graphene.ClientIDMutation):
    class Input:
        client = graphene.Argument(ClientInput)

    client = graphene.Field(Client)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        node = kwargs.get('node')
        client = models.Client(user=info.context.user)
        [setattr(client, key, val) for key, val in node.items()]
        client.save()
        return ClientAdd(client=client)


class ClientUpdate(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)
        client = graphene.Argument(ClientInput)

    client = graphene.Field(Client)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        client_id = from_global_id(id)[1]
        node = kwargs.get('node')
        client = models.Client.objects.get(id=client_id, user=info.context.user)
        [setattr(client, key, val) for key, val in node.items()]
        client.save()
        return ClientUpdate(client=client)


class ClientDelete(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)

    client = graphene.Field(Client)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        client_id = from_global_id(id)[1]
        client = models.Client.objects.get(id=client_id, user=info.context.user)
        client.delete()
        return ClientDelete(client=client)

# --------------------------------------------------
# -- INVOICE
# --------------------------------------------------


class Invoice(DjangoObjectType):
    class Meta:
        model = models.Invoice
        interfaces = (graphene.Node, )
        filter_fields = {
            'name': ['exact', 'icontains', 'istartswith'],
            'city': ['exact', 'icontains', 'istartswith'],
            'type': ['exact', 'icontains', 'istartswith'],
        }


class InvoiceInput(graphene.InputObjectType):
    name = graphene.String()
    city = graphene.String()
    type = graphene.String()
    seller_id = graphene.String()
    buyer_id = graphene.String()


class InvoiceAdd(graphene.ClientIDMutation):
    class Input:
        invoice = graphene.Argument(InvoiceInput)

    invoice = graphene.Field(Invoice)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        node = kwargs.get('node')
        seller_id = node.seller_id
        buyer_id = node.buyer_id

        seller_id = from_global_id(seller_id)[1]
        buyer_id = from_global_id(buyer_id)[1]

        seller = models.Client.objects.get(id=seller_id)
        buyer = models.Client.objects.get(id=buyer_id)

        invoice = models.Invoice(user=info.context.user)

        [setattr(invoice, key, val) for key, val in node.items() if key not in ('seller_id', 'buyer_id')]

        invoice.seller = seller
        invoice.buyer = buyer

        invoice.save()
        return InvoiceAdd(invoice=invoice)


class InvoiceUpdate(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)
        invoice = graphene.Argument(InvoiceInput)

    invoice = graphene.Field(Invoice)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        invoice_id = from_global_id(id)[1]
        node = kwargs.get('node')

        invoice = models.Invoice.objects.get(id=invoice_id, user=info.context.user)
        
        seller_id = node.seller_id
        buyer_id = node.buyer_id

        seller_id = from_global_id(seller_id)[1]
        buyer_id = from_global_id(buyer_id)[1]

        seller = models.Client.objects.get(id=seller_id)
        buyer = models.Client.objects.get(id=buyer_id)

        [setattr(invoice, key, val) for key, val in node.items() if key not in ('seller_id', 'buyer_id')]

        invoice.seller = seller
        invoice.buyer = buyer
        
        invoice.save()
        return InvoiceUpdate(invoice=invoice)


class InvoiceDelete(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)

    invoice = graphene.Field(Invoice)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        invoice_id = from_global_id(id)[1]
        invoice = models.Invoice.objects.get(id=invoice_id, user=info.context.user)
        invoice.delete()
        return InvoiceDelete(invoice=invoice)

# --------------------------------------------------
# -- COMMODITY
# --------------------------------------------------


class Commodity(DjangoObjectType):
    class Meta:
        model = models.Commodity
        interfaces = (graphene.Node, )
        filter_fields = {
            'name': ['exact', 'icontains', 'istartswith'],
            'jm': ['exact', 'icontains', 'istartswith'],
            'pkwiu': ['exact', 'icontains', 'istartswith'],
            'net': ['exact', 'gt', 'lt', 'gte', 'lte'],
            'vat': ['exact'],
        }


class CommodityInput(graphene.InputObjectType):
    name = graphene.String()
    jm = graphene.String()
    pkwiu = graphene.String()
    net = graphene.String()
    vat = graphene.String()


class CommodityAdd(graphene.ClientIDMutation):
    class Input:
        commodity = graphene.Argument(CommodityInput)

    commodity = graphene.Field(Commodity)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        node = kwargs.get('client')
        commodity = models.Commodity(user=info.context.user)
        [setattr(commodity, key, val) for key, val in node.items() if key not in ('vat', 'net')]
        commodity.net = decimal.Decimal(node.net)
        commodity.vat = decimal.Decimal(node.vat)
        commodity.save()
        return CommodityAdd(commodity=commodity)


class CommodityUpdate(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)
        commodity = graphene.Argument(CommodityInput)

    commodity = graphene.Field(Commodity)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        commodity_id = from_global_id(id)[1]
        node = kwargs.get('node')
        commodity = models.Commodity.objects.get(id=commodity_id, user=info.context.user)
        [setattr(commodity, key, val) for key, val in node.items() if key not in ('vat', 'net')]
        commodity.net = decimal.Decimal(node.net)
        commodity.vat = decimal.Decimal(node.vat)
        commodity.save()
        return CommodityUpdate(commodity=commodity)


class CommodityDelete(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)

    commodity = graphene.Field(Commodity)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        commodity_id = from_global_id(id)[1]
        commodity = models.Commodity.objects.get(id=commodity_id, user=info.context.user)
        commodity.delete()
        return CommodityDelete(commodity=commodity)

# --------------------------------------------------
# -- INVOICE DATE
# --------------------------------------------------


class InvoiceDate(DjangoObjectType):
    class Meta:
        model = models.InvoiceDate
        interfaces = (graphene.Node, )
        filter_fields = {
            'make_time': ['exact', 'gt', 'lt', 'gte', 'lte'],
            'end_time': ['exact', 'gt', 'lt', 'gte', 'lte'],
            'pay_time': ['exact', 'gt', 'lt', 'gte', 'lte'],
        }


class InvoiceDateInput(graphene.InputObjectType):
    make_time = graphene.DateTime()
    end_time = graphene.DateTime()
    pay_time = graphene.DateTime()


class InvoiceDateAdd(graphene.ClientIDMutation):
    class Input:
        invoice_date = graphene.Argument(InvoiceDateInput)

    invoice_date = graphene.Field(InvoiceDate)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        node = kwargs.get('node')
        invoice_date = models.InvoiceDate(user=info.context.user)
        [setattr(invoice_date, key, val) for key, val in node.items()]
        invoice_date.save()
        return InvoiceDateAdd(invoice_date=invoice_date)


class InvoiceDateUpdate(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)
        invoice_date = graphene.Argument(InvoiceDateInput)

    invoice_date = graphene.Field(InvoiceDate)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        invoice_date_id = from_global_id(id)[1]
        node = kwargs.get('node')
        invoice_date = models.InvoiceDate.objects.get(id=invoice_date_id, user=info.context.user)
        [setattr(invoice_date, key, val) for key, val in node.items()]
        invoice_date.save()
        return InvoiceDateUpdate(invoice_date=invoice_date)


class InvoiceDateDelete(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)

    invoice_date = graphene.Field(InvoiceDate)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        invoice_date_id = from_global_id(id)[1]
        invoice_date = models.InvoiceDate.objects.get(id=invoice_date_id, user=info.context.user)
        invoice_date.delete()
        return InvoiceDateDelete(invoice_date=invoice_date)

# --------------------------------------------------
# -- INVOICE COMMODITY
# --------------------------------------------------


class InvoiceCommodity(DjangoObjectType):
    class Meta:
        model = models.InvoiceCommodity
        interfaces = (graphene.Node, )
        filter_fields = {
            'date': ['exact', 'gt', 'lt', 'gte', 'lte'],
            'uid': ['exact', 'icontains', 'istartswith'],
            'quantity': ['exact', 'gt', 'lt', 'gte', 'lte'],
            'pay_time': ['exact', 'gt', 'lt', 'gte', 'lte'],
            'pay_jm': ['exact', 'icontains', 'istartswith'],
        }


class InvoiceCommodityInput(graphene.InputObjectType):
    date_id = graphene.String()
    uid = graphene.String()
    quantity = graphene.String()
    pay_time = graphene.Int()
    pay_jm = graphene.String()
    title = graphene.String()
    invoice_id = graphene.String()
    commodity_id = graphene.String()


class InvoiceCommodityAdd(graphene.ClientIDMutation):
    class Input:
        invoice_commodity = graphene.Argument(InvoiceCommodityInput)

    invoice_commodity = graphene.Field(Invoice)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        node = kwargs.get('node')
        date_id = node.date_id
        invoice_id = node.invoice_id
        commodity_id = node.commodity_id

        date_id = from_global_id(date_id)[1]
        invoice_id = from_global_id(invoice_id)[1]
        commodity_id = from_global_id(commodity_id)[1]

        date = models.InvoiceDate.objects.get(id=date_id)
        invoice = models.Invoice.objects.get(id=invoice_id)
        commodity = models.Invoice.objects.get(id=commodity_id)

        invoice_commodity = models.InvoiceCommodity(user=info.context.user)

        [setattr(invoice_commodity, key, val) for key, val in node.items() if key not in ('date_id', 'invoice_id', 'commodity_id')]

        invoice_commodity.date = date
        invoice_commodity.invoice = invoice
        invoice_commodity.commodity = commodity

        invoice_commodity.save()
        return InvoiceCommodityAdd(invoice_commodity=invoice_commodity)


class InvoiceCommodityUpdate(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)
        invoice_commodity = graphene.Argument(InvoiceCommodityInput)

    invoice_commodity = graphene.Field(Invoice)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        invoice_commodity_id = from_global_id(id)[1]
        node = kwargs.get('node')

        date_id = node.date_id
        invoice_id = node.invoice_id
        commodity_id = node.commodity_id

        date_id = from_global_id(date_id)[1]
        invoice_id = from_global_id(invoice_id)[1]
        commodity_id = from_global_id(commodity_id)[1]

        date = models.InvoiceDate.objects.get(id=date_id)
        invoice = models.Invoice.objects.get(id=invoice_id)
        commodity = models.Invoice.objects.get(id=commodity_id)

        invoice_commodity = models.InvoiceCommodity.objects.get(id=invoice_commodity_id, user=info.context.user)

        [setattr(invoice_commodity, key, val) for key, val in node.items() if key not in ('date_id', 'invoice_id', 'commodity_id')]

        invoice_commodity.date = date
        invoice_commodity.invoice = invoice
        invoice_commodity.commodity = commodity

        invoice_commodity.save()
        return InvoiceCommodityUpdate(invoice_commodity=invoice_commodity)


class InvoiceCommodityDelete(graphene.ClientIDMutation):
    class Input:
        id = graphene.String(required=True)

    invoice_commodity = graphene.Field(InvoiceCommodity)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **kwargs):
        id = kwargs.get('id')
        invoice_commodity_id = from_global_id(id)[1]
        invoice_commodity = models.InvoiceCommodity.objects.get(id=invoice_commodity_id, user=info.context.user)
        invoice_commodity.delete()
        return InvoiceDelete(invoice_commodity=invoice_commodity)

# --------------------------------------------------
# -- QUERY
# --------------------------------------------------


class Query(graphene.ObjectType):
    client_list = DjangoFilterConnectionField(Client,
                                              start=graphene.Int(),
                                              end=graphene.Int())
    invoice_list = DjangoFilterConnectionField(Invoice,
                                 start=graphene.Int(),
                                 end=graphene.Int())
    commoditiy_list = DjangoFilterConnectionField(Commodity,
                                    start=graphene.Int(),
                                    end=graphene.Int())
    invoice_date_list = DjangoFilterConnectionField(InvoiceDate,
                                      start=graphene.Int(),
                                      end=graphene.Int())
    invoice_commodity_list = DjangoFilterConnectionField(InvoiceCommodity,
                                           start=graphene.Int(),
                                           end=graphene.Int())
    
    client = graphene.Field(Client, id=graphene.Int())
    invoice = graphene.Field(Invoice, id=graphene.Int())
    commodity = graphene.Field(Commodity, id=graphene.Int())
    invoice_date = graphene.Field(InvoiceDate, id=graphene.Int())
    invoice_commodity = graphene.Field(InvoiceCommodity, id=graphene.Int())

    def resolve_client_list(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.Client.objects.none()
        start = kwargs.get('start')
        end = kwargs.get('end')
        if start is not None and end is not None:
            return models.Client.objects.filter(user=info.context.user)[start:end]
        return models.Client.objects.filter(user=info.context.user)

    def resolve_invoice_list(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.Invoice.objects.none()
        start = kwargs.get('start') or 0
        end = kwargs.get('end') or 10
        if start is not None and end is not None:
            return models.Invoice.objects.filter(user=info.context.user)[start:end]
        return models.Invoice.objects.filter(user=info.context.user)

    def resolve_commoditiy_list(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.Commodity.objects.none()
        start = kwargs.get('start') or 0
        end = kwargs.get('end') or 10
        if start is not None and end is not None:
            return models.Commodity.objects.filter(user=info.context.user)[start:end]
        return models.Commodity.objects.filter(user=info.context.user)

    def resolve_invoice_date_list(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.InvoiceDate.objects.none()
        start = kwargs.get('start') or 0
        end = kwargs.get('end') or 10
        if start is not None and end is not None:
            return models.InvoiceDate.objects.filter(user=info.context.user)[start:end]
        return models.InvoiceDate.objects.filter(user=info.context.user)

    def resolve_invoice_commodity_list(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.InvoiceCommodity.objects.none()
        start = kwargs.get('start') or 0
        end = kwargs.get('end') or 10
        if start is not None and end is not None:
            return models.InvoiceCommodity.objects.filter(user=info.context.user)[start:end]
        return models.InvoiceCommodity.objects.filter(user=info.context.user)

    def resolve_client(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.Client.objects.none()
        id = kwargs.get('id')
        if id is not None:
            return models.Client.objects.get(id=id)
        return models.Client.objects.none()

    def resolve_invoice(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.Invoice.objects.none()
        id = kwargs.get('id')
        if id is not None:
            return models.Invoice.objects.get(id=id, user=info.context.user)
        return models.Invoice.objects.none()

    def resolve_commodity(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.Commodity.objects.none()
        id = kwargs.get('id')
        if id is not None:
            return models.Commodity.objects.get(id=id, user=info.context.user)
        return models.Commodity.objects.none()

    def resolve_invoice_date(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.InvoiceDate.objects.none()
        id = kwargs.get('id')
        if id is not None:
            return models.InvoiceDate.objects.get(id=id, user=info.context.user)
        return models.InvoiceDate.objects.none()

    def resolve_invoice_commodity(self, info, **kwargs):
        if not info.context.user.is_authenticated:
            return models.InvoiceCommodity.objects.none()
        id = kwargs.get('id')
        if id is not None:
            return models.InvoiceCommodity.objects.get(id=id, user=info.context.user)
        return models.InvoiceCommodity.objects.none()

# --------------------------------------------------
# -- MUTATION
# --------------------------------------------------


class QueryMutation(graphene.ObjectType):
    client_add = ClientAdd.Field()
    client_update = ClientUpdate.Field()
    client_delete = ClientDelete.Field()

    invoice_add = InvoiceAdd.Field()
    invoice_updae = InvoiceUpdate.Field()
    invoice_delete = InvoiceDelete.Field()

    commodity_add = CommodityAdd.Field()
    commodity_update = CommodityUpdate.Field()
    commodity_delete = CommodityDelete.Field()

    invoice_date_add = InvoiceDateAdd.Field()
    invoice_date_update = InvoiceDateUpdate.Field()
    invoice_date_delete = InvoiceDateDelete.Field()

    invoice_commodity_add = InvoiceCommodityAdd.Field()
    invoice_commodity_update = InvoiceCommodityUpdate.Field()
    invoice_commodity_delete = InvoiceCommodityDelete.Field()


schema = graphene.Schema(query=Query, mutation=QueryMutation)
