#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json
import uuid
import os

ADDRESS = os.environ['PDF_SERVER'] or 'http://localhost:8888/api'
TEST_JSON = 'data/invoice-01.json'


def generate_invoice(data):
    data = json.loads(data)
    result = requests.post(ADDRESS, json=data)
    return result.content


def generate_test_invoice():
    with open(TEST_JSON) as f:
        data = f.read()
    return generate_invoice(data=data)


if __name__ == '__main__':
    content = generate_test_invoice()
    name = '{}.pdf'.format(str(uuid.uuid4()))
    with open(name, 'wb') as f:
        f.write(content)
    pass