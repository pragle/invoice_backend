#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Client(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.DO_NOTHING, default=0)
    name = models.CharField(max_length=255, help_text='Name of client',)
    signature = models.CharField(max_length=255, help_text='Signature written on bottom',)
    data = models.TextField(help_text='Data required on invoice')
    bank_account = models.CharField(max_length=255, help_text='Bank account number')
    
    class Meta:
        ordering = ['pk']

    def __str__(self):
        return "{}".format(self.name)


class Invoice(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.DO_NOTHING, default=0)
    seller = models.ForeignKey(to=Client, on_delete=models.DO_NOTHING, related_name='seller')
    buyer = models.ForeignKey(to=Client, on_delete=models.DO_NOTHING, related_name='buyer')
    name = models.CharField(max_length=128, help_text='Name of invoice for easy search')
    city = models.CharField(max_length=128, help_text='City where inovice is created')
    type = models.CharField(max_length=128, help_text='Type of invoice ex vat')

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return "{}".format(self.name)


class Commodity(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.DO_NOTHING, default=0)
    name = models.CharField(max_length=128, default='', help_text='Name of commodity on invoice')
    jm = models.CharField(max_length=30, help_text='Counting name')
    pkwiu = models.CharField(max_length=30, help_text='PKWiU for polish invoice')
    net = models.DecimalField(decimal_places=2, max_digits=100, help_text='Net value of commodity')
    vat = models.DecimalField(decimal_places=2, max_digits=100, help_text='Vat value in percent')

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return "{} {} {}".format(self.name, self.jm, self.net)


class InvoiceDate(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.DO_NOTHING, default=0)
    make_time = models.DateTimeField(help_text='Creation date on invoice')
    end_time = models.DateTimeField(help_text='End of service date')
    pay_time = models.DateTimeField(help_text='Pay date')

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return "service date : {:%Y-%m-%d} invoice date : {:%Y-%m-%d} due date : {:%Y-%m-%d}"\
            .format(self.make_time, self.end_time, self.pay_time)


class InvoiceCommodity(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.DO_NOTHING, default=0)
    date = models.ForeignKey(to=InvoiceDate, on_delete=models.DO_NOTHING)
    uid = models.CharField(max_length=128, help_text='Inovice uid ex 1,2,3')
    quantity = models.DecimalField(decimal_places=2, max_digits=100, help_text='Number of items')
    pay_time = models.IntegerField(help_text='Pay time in days')
    pay_jm = models.CharField(max_length=10, help_text='Pay jm')
    title = models.CharField(max_length=255, default='', help_text='Commodity title for invoice')
    invoice = models.ForeignKey(to=Invoice, on_delete=models.DO_NOTHING)
    commodity = models.ForeignKey(to=Commodity, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['pk']

    def __str__(self):
        net = self.quantity*self.commodity.net
        brutto = net * (1+(self.commodity.vat / 100))
        return "{} {} {}/{:%m/%Y} net {:.2f} gross {:.2f}"\
            .format(self.invoice.name, self.title, self.uid, self.date.end_time, net, brutto)
