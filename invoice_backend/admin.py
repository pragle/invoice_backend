#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from invoice_backend import models

admin.site.register(models.Client)
admin.site.register(models.Invoice)
admin.site.register(models.Commodity)
admin.site.register(models.InvoiceDate)
admin.site.register(models.InvoiceCommodity)