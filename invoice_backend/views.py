#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.http import HttpResponse
from invoice_backend import invoice_pdf
import uuid


def generate_invoice(request):
    if request.method == 'POST':
        print("{}".format(request.body))
        content = invoice_pdf.generate_invoice(request.body)
    else:
        content = invoice_pdf.generate_test_invoice()
    response = HttpResponse(content=content, content_type="application/pdf")
    response["Content-Disposition"] = "inline; filename={}.pdf".format(str(uuid.uuid4()))
    return response