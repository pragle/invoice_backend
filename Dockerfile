FROM python:3.6-alpine
MAINTAINER michal@vane.pl
WORKDIR /
RUN apk --no-cache add bash
COPY . /invoice_backend
WORKDIR /invoice_backend
RUN pip install -r requirements.txt
EXPOSE 8000
CMD python manage.py runserver 0.0.0.0:8000